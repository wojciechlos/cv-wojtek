<?php 

namespace Project\Node;

/**
 * Node
 *
 * @author Wojtunioo
 * @date 2018-12-05
 */
class Node {
    public function __construct($letter, $parent, $end){

        $this->letter = $letter;
        $this->parent = $parent;
        $this->end_word = $end;
        $this->nodes = [];
    }

    public function addNode($node){
        $this->nodes[] = $node;
    }

    public function getNodes()
    {
        return $this->nodes;
    }

    public function getNode($letter){
        foreach($this->nodes as $node)
        {
            if ($node->getLetter($letter) == $letter)
            {
                return $node;
            }
            return false;
        }
    }

    public function getParent(){
        return $this->parent;
    }

    public function isEndWord(){
        return $this->end_word;
    }

    public function setEndWord(){
        $this->end_word = true;
    }

    public function getLetter(){
        return $this->letter;
    }
}

?>