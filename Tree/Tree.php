<?php 

namespace Project\Tree;

/**
 * Tree
 *
 * @author Wojtunioo
 * @date 2018-12-05
 */
use Project\Node\Node;

class Tree {

    public function __construct($file) {
        
        $book = array($file, FILE_IGNORE_NEW_LINES);
        $words =[];

        foreach($book as $line)
        {
            $word = explode(' ', $line);
            foreach($word as $w)
            {
                array_push($words, $w);
            }
            
        }

        $words = array_unique($words);
        $texts = $words;
        $this->nodes = [];

        foreach($texts as $t)
        {
            for ($i=0; $i<strlen($t); $i++){
                if ($i == 0)
                {
                    $this->firstLetter($t[$i]);
                }
                else {
                    $this->addAnotherLetter($t, $i);
                }
            }
        }
    }

    public function firstLetter($letter){
        $new_letter = true;
        foreach ($this->nodes as $node)
        {
            if ($node->getLetter() == $letter)
            {
                $new_letter = false;
            }
        }
        if ($new_letter)
        {
            $this->nodes[] = new Node($letter, false, false);
        }
    }
    public function addAnotherLetter($word, $position){

        $cur_pos = 0;
        $node = false;
        $nodes = $this->nodes;

        while($cur_pos != $position)
        {
            forEach($nodes as $node)
            {
                if ($node->getLetter() == $word[$cur_pos])
                {
                    $cur_node = $node;
                    $nodes = $node->getNodes();
                }
            }
            $cur_pos++;
        }

        $new_letter = true;

        foreach ($nodes as $node)
        {
            if ($node->getLetter() == $word[$position])
            {
                $new_letter = false;
                $end_node = $node;
            }
        }

        if ($new_letter)
        {
            $end = $cur_pos + 1  == strlen($word);
            $cur_node->addNode(new Node($word[$position], $cur_node, $end));
        }

        else {

            if ( $cur_pos +1  == strlen($word)  )
            {
                $end_node->setEndWord();
            }
        }
        
    }
}

?>