<?php 

namespace Project\Finder;

use Project\Tree\Tree;

/**
 * Finder
 *
 * @author Wojtunioo
 * @date 2018-12-05
 */
class Finder {

    public function __construct($file)
    {
        $this->tree = new Tree($file);
    }
    public function findWords(String $word)
    {
        $this->word = $word;
        
        $finalNodes = $this->getFinalNodes($word);
        
        $last = [];
        foreach($finalNodes as $node)
        {
            array_push($last, $node->getLetter());
        }
        $end_words = [];
        foreach($finalNodes as $node)
        {
            if($node->end_word)
            {
                $end_words[] = $this->rebildWord($node);
            }
        }
        return $end_words;
    }
    public function getFinalNodes($word) {

        $cur_pos = 0;
        $nodes = $this->tree->nodes;

        $end_nodes = [];

        while($cur_pos != strlen($word))
        {
            if ($word[$cur_pos] != '*')
            {
                $temp_nodes = [];
                $found_node = false;
                foreach($nodes as $node)
                {
                    if ($node->getLetter() == $word[$cur_pos])
                    {
                        if($cur_pos + 1 == strlen($word))
                        {
                            $end_nodes[] = $node;
                        }
                        $curent_nodes = $node->getNodes();
                        foreach ($curent_nodes as $n)
                        {
                            array_push($temp_nodes, $n);
                        }
                        $found_node = true;
                    }
                }
                if(!$found_node)
                {
                    return 'No words Found';
                }
                else {
                    $nodes = $temp_nodes;
                }
            }
            else {
                $temp_nodes = [];
                forEach($nodes as $node)
                {
                    if($cur_pos + 1 == strlen($word))
                    {
                        $end_nodes[] = $node;
                    }
                    $t = $node->getNodes();

                    foreach($t as $e)
                    {
                        array_push($temp_nodes, $e);
                    }
                }
                $nodes = $temp_nodes;
            }
            $cur_pos++;
        }
        return $end_nodes;
    }

    public function rebildWord($node) {

        $end = false;
        $word = $node->getLetter();

        while($end == false)
        {
            $node = $node->getParent();
            $word .= $node->getLetter();
            if($node->parent == false)
            {

                $end = true;
            }
        }
        return strrev($word);
    }
}

?>