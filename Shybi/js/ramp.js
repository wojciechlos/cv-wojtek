class Ramps {
	constructor(ramps)
	{
		this.ramps = ramps;
		//this.grid = grid;
		
		 this.canvas = document.getElementById('ramps'),
		 this.ctx = this.canvas.getContext('2d');
		 
		 this.img = new Image();
		 this.img.src = 'Shybi/img/groundBox 1.png';
		 
		 this.movingramp = ''
		 
		 var me = this;
		 this.position = 0;
		 this.frame = 0;
		 this.asc = true;
		 this.img.onload = function()
		 {
			setInterval(function()
			{
				me.spriteDraw(); 
			},10);				
			
		 }
	}
	spriteDraw()
	{
		var me = this;
		me.ctx.clearRect(0, 0, 800, 500);
		//var floors = [];
		//   floor  (left, up, width);
		this.ramps.forEach(function(ramp)
		{
			me.ctx.moveTo(0, 0);
			me.ctx.drawImage(me.img, 30*me.position,0,30,30,ramp[0],ramp[1]-7, 30,30);	
			//var floor = [ramp[0],ramp[1],150];
			//floors.push(floor);
			
		}); 
	
				me.frame++
				
				if(me.frame == 300) me.frame = 0;
				if(me.frame % 10 == 0)
				{
					me.position++;
				if(me.position == 3) me.position = 0;
				}
				
			
			
		
	}
	moveRamp(side, pos)
	{
		
		clearInterval(this.movingramp);
		var me = this;
		this.movingramp = setInterval(function()
		{
			if(side == 'right')
			{
				me.ramps.forEach(function(ramp)
				{
					ramp[0] = ramp[0]-pos/4;
				});
			}
			else
			{
				me.ramps.forEach(function(ramp)
				{
					ramp[0] = ramp[0]+pos/4;
				});
			}
		},10);
		
	}
}