class Grid {
	constructor(hero, ramps)
	{
		this.hero = hero;
		this.ramps = ramps;
		this.gravity = 1;
		this.jumpRate = 18;
		this.plaingGame = true;
		this.heroOnRamp = false;
		this.herojumps = false;
		this.newMove = false;
		this.side = 'idle';
		
		this.gridPosition = 400;
		
		this.startGame();
	}
	startGame()
	{
		var me = this;
		var game = setInterval(function()
		{
			//console.log(me.heroJumps);
			me.checkHero()
			if(!me.heroOnRamp && !me.heroJumps)
			{
						
				me.hero.heroFalling(me.gravity);
				me.gravity = me.gravity+0.2;
				//console.log(me.gravity)	
			}
			if(me.heroJumps)
			{
				me.hero.heroJump(-me.jumpRate);
				me.jumpRate--;
				if(me.jumpRate == 0)
				{
					me.heroEndJump();
				}
			}
			var first = false;
			if(me.side == 'left' && me.gridPosition != 300)
			{
				me.gridPosition = me.gridPosition - 1;
				first = true;
			}
			if(me.side == 'right' && me.gridPosition != 430)
			{
				me.gridPosition = me.gridPosition + 1;
				first = true;
			}
			if(me.gridPosition == 430 && me.side == 'right' && first)
			{
				//console.log('tu sie powinno zadziać');
				me.newMove = true;
				me.makeMove(0);
				me.ramps.moveRamp('right', 10);
			}
			if(me.gridPosition == 300 && me.side == 'left' && first)
			{
				//console.log('tu sie powinno zadziać');
				me.newMove = true;
				me.makeMove(0);
				me.ramps.moveRamp('left', 10);
			}
			
			me.makeMove(10);
			
			
			
			//console.log(me.gridPosition);
			
			//console.log('ello');
		},10);
	}
	checkHero()
	{
		var me = this;
		me.heroOnRamp = false;
		this.ramps.ramps.forEach(function(ramp)
		{
			//console.log(ramp[1]);
			
			if((me.hero.posX+64 >= ramp[0] && me.hero.posX <= (ramp[0]+30)) &&  ((ramp[1] - me.hero.posY-64-me.gravity ) <= 1 && (ramp[1] - me.hero.posY-64 ) >=0 ))
			{
				
				//me.hero.posY = me.hero.posY;
				//console.log('ello');
				me.heroOnRamp = true;
				me.gravity = 1;
				//return false;
			}
			//else
			//{
			//	me.heroOnRamp = false;	
			//}				
				
			
			
		});
	}
	heroJump()
	{
		this.heroJumps = true;
	/*	var i;
		for(i=0; i < 100 ; i++)
		{
			this.heroJumps = true;
			this.hero.heroJump(-(i*0.03));
		}
		this.heroEndJump();*/
		//console.log(this.jumpRate); 
		
		//this.heroOnRamp = false;
	}
	heroEndJump()
	{
		this.heroJumps = false;
		this.jumpRate = 18;
	}
	right()
	{
		this.newMove = true;
	    this.side = 'right';
	}
	left()
	{
		this.side = 'left';
		this.newMove = true;
	}
	makeMove(val)
	{
		
		if(this.newMove)
			{
				if(val == 10)
				{
					this.ramps.moveRamp('right', 0);
				}
				
				if(this.side == 'right')
				{
					this.hero.heroRunRight(val);
					this.newMove = false;
					
				}
				
				if(this.side == 'left')
				{
					this.hero.heroRunLeft(val);
					this.newMove = false;
				}
				
			}
	}
}