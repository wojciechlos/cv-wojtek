class Hero {
	constructor()
	{
		 
		 this.posX = 400;
		 this.posY = 0;
		
		 this.action = '';	
		 this.animation = '';
		 this.move = '';
		
		 this.canvas = document.getElementById('hero'),
		 this.ctx = this.canvas.getContext('2d');
		 
		 this.heroIdle = new Image();
		 this.heroIdle.src = 'Shybi/img/hero/idle.png';
		 
		 this.heroRunRightImg = new Image();
		 this.heroRunRightImg.src = 'Shybi/img/hero/walkRight.png';
		 
		 this.heroRunLeftImg = new Image();
		 this.heroRunLeftImg.src = 'Shybi/img/hero/walkLeft.png';
		 
		 var me = this;
		 
		 this.gameFrame = 0;
		 this.frame = 0;
		 
		  this.heroIdle.onload = function(){
				me.heroWaitng();
			
		};
	}
	drawHero(action, val)
	{
		
		if(action == 'idle')
		{
			
			//console.log(Math.round(this.gameFrame/16));
			this.ctx.clearRect(0, 0, 800, 500);
			this.ctx.drawImage(this.heroIdle, Math.round(this.gameFrame)*64, 0, 64,64,this.posX,this.posY, 64, 64);
			
			this.frame++;
			if(this.frame % 10 == 0)
			{
				this.gameFrame++;
				if(this.gameFrame == 7) this.gameFrame = 0;
			}
			if(this.frame ==100) this.frame =0;
			
			//console.log(this);
		}
		if(action == 'runRight')
		{
			this.ctx.clearRect(0, 0, 800, 500);
			this.ctx.drawImage(this.heroRunRightImg, this.gameFrame*64, 0, 64,64,this.posX,this.posY, 64, 64);
			this.frame++;
			this.posX+=+val/4;
			if(this.frame % 4 == 0)
			{
				
				this.gameFrame++;
				if(this.gameFrame == 8) this.gameFrame = 0;
			}
			if(this.frame == 100) this.frame =0;
			
			//console.log('r');
		}
		if(action == 'runLeft')
		{
			this.ctx.clearRect(0, 0, 800, 500);
			this.ctx.drawImage(this.heroRunLeftImg, this.gameFrame*64, 0, 64,64,this.posX,this.posY, 64, 64);
			this.frame++;
			this.posX+=-val/4;
			if(this.frame % 4 == 0)
			{
				
				this.gameFrame++;
				if(this.gameFrame == 8) this.gameFrame = 0;
			}
			if(this.frame == 100) this.frame =0;
			//console.log('l');
		}
		//this.heroFalling();
		//var legs = [this.posX, this.posY + 120, 70];
		// Legs [left, up + 120, width = 70]
		//this.grid.updateHeroLegs(legs);
	}
	heroWaitng()
	{
		this.gameFrame = 0;
		//console.log('idle');
		var me = this;
		//if(this.action != 'idle')
		clearInterval(this.animation);
		this.action = 'idle';
		this.animation = setInterval(function()
		{
			me.drawHero('idle');
			
		},10);
	}
	heroRunRight(val)
	{
		this.frame = 0;
		this.gameFrame = 0;
		//console.log('run');
		var me = this;
		//if(this.action != 'runRight')
		clearInterval(this.animation);
		this.action = 'runRight';
		this.animation = setInterval(function()
		{
			me.drawHero('runRight', val);
			
		},10);
		
	}
	heroRunLeft(val)
	{
		this.frame = 0;
		this.gameFrame = 0;
		//console.log('run');
		var me = this;
		//if(this.action != 'runLeft')
		clearInterval(this.animation);
		this.action = 'runLeft';
		this.animation = setInterval(function()
		{
			me.drawHero('runLeft', val);
			
		},10);
		
	}
	heroJump(val)
	{
		this.posY = this.posY + val;
		/*var jumpArr = [];
		var r;
		for(r=30;r>0;r--)
		{
			jumpArr.push(r*r);
		};
		var me = this;
		var i = 0;
		var jump = setInterval(function(){
			
			me.posY+=-jumpArr[i]*0.02;
				
			if(i == 29) clearInterval(jump);
			
			i++;
		},80); */
		
	}
	heroFalling(val)
	{
		
		this.posY = this.posY+val;
	}
	
	

	
}