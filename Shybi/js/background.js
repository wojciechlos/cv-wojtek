class Background {	
	
	constructor()
	{
		 this.canvas = document.getElementById('background'),
		 this.ctx = this.canvas.getContext('2d');
		 
		 this.backgroundImg = new Image();
		 this.backgroundImg.src = 'Shybi/img/background.png';
			  
			 
		  var me = this;
		  this.backgroundImg.onload = function(){
			
			me.moment = 0;
			setInterval(function()
			{
				me.drawingBackground();
			},50);
			
		};
		
	}
	drawingBackground()
	{
		this.ctx.clearRect(0, 0, 800, 500);
		this.ctx.drawImage(this.backgroundImg, this.moment,0,800,500,0,0,800,500);
		     
	    this.moment++;
		if(	this.moment == 1400)
		{
			this.moment = 0;
		}
	}	
}

