<?php

namespace Project\Utilities\CreateErrorStatus;
/**
 * Generates Error Status 
 * 
 * @author Wojtunioo
 * @date 2018-12-05
 */
class CreateErrorStatus {
    public function __construct($code, $message) {
        http_response_code($code);
        echo json_encode(["status" => false, "message" => $message]);
        exit;
    }
}
?>