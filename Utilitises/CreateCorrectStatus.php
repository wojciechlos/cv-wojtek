<?php 

namespace Project\Utilities\CreateCorrectStatus;

/**
 * Generates Correct Status
 *
 * @author Wojtunioo
 * @date 2018-12-05
 */
class CreateCorrectStatus {
    public function __construct($data) {
        http_response_code(200);
        echo json_encode(["status" => true, "data" => $data]);
        exit;
    }
}
?>