<?php 

namespace Project;

include ('Utilitises/CreateCorrectStatus.php');
include ('Utilitises/CreateErrorStatus.php');
include ('Node/Node.php');
include ('Tree/Tree.php');
include ('Finder/Finder.php');

use Project\Finder\Finder;
use Project\Utilities\CreateCorrectStatus\CreateCorrectStatus;
use Project\Utilities\CreateErrorStatus\CreateErrorStatus;

header ("Access-Control-Allow-Origin: *");
header ("Content-Type: application/json; charset=UTF-8");
header ("Access-Control-Allow-Methods: POST");
header ("Access-Control-Max-Age: 3600");
header ("Access-Control-Allow-Headers: Content-Type, Access-Control-Allow-Headers, Authorization, X-Requested-With");

if (session_status() == PHP_SESSION_NONE) {
    session_start();
}

if (!isset($_POST['action']))
{
    return new CreateErrorStatus(406, "There were no action send");
}

switch ($_POST['action']) {
    // UPDATE DICTIONARY
    case 'update-dictionary':
            $fileContent = file_get_contents($_FILES['thefile']['tmp_name']);
            $_SESSION['tree'] = new Finder($fileContent);
        return new CreateCorrectStatus('wojtek');
        break;
    // FINDS WORDS IN DICTIONARY
    case 'find-word' :
        if(!isset($_SESSION['tree']))
        {
            return new CreateErrorStatus(406, "Dictionary not send");
        }
        if(empty($_POST['word']))
        {
            return new CreateErrorStatus(406, "No word send");
        }
        $finder = $_SESSION['tree'];
        $words = $finder->findWords($_POST['word']);
        return new CreateCorrectStatus($words);
        break;
    default:
        new CreateErrorStatus(405, "Ther is no action called: ".$_POST['action']);
        break;
}

